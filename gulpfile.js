const gulp  = require('gulp');
const browserSync = require('browser-sync').create();

gulp.task('copyHtml',function(){
    return gulp.src(['src/*.html'])
        .pipe(gulp.dest('dest'));
})

gulp.task('copyCss',function(){
    return gulp.src(['src/css/*.css'])
      .pipe(gulp.dest('dest/css'))
});

gulp.task('copyJs',function(){
    return gulp.src(['src/js/*.js'])
      .pipe(gulp.dest('dest/js'))
})

gulp.task('serve',['copyCss','copyJs','copyHtml'],function(){
    browserSync.init({
        server: './dest'
    });
    gulp.watch(['src/css/*.css'],['copyCss']).on('change',browserSync.reload);
    gulp.watch(['src/js/*.js'],['copyJs']).on('change',browserSync.reload);
    gulp.watch(['src/*.html'],['copyHtml']).on('change',browserSync.reload);
});

//Default task
gulp.task('default',['serve']);